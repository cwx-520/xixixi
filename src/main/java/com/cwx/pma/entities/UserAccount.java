package com.cwx.pma.entities;


import javax.persistence.*;

@Entity
@Table(name = "user_accounts")
public class UserAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "user_accounts_seq")
    @SequenceGenerator(name="user_accounts_seq",sequenceName = "user_accounts_seq", allocationSize = 1)
    private long userId;
    @Column(name = "username")
    private String userName;

    private  String email;

    private  String password;

    private boolean enabled = true;
    @Override
    public String toString() {
        return "UserAccount{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                '}';
    }



    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }



    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public long getUserId() {
        return userId;
    }
}
