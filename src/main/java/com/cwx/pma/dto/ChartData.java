package com.cwx.pma.dto;

public interface ChartData {

    String getLabel();
    long getValue();
}
