package com.cwx.pma.dto;

public interface StudentProject {

    String getName();
    String getWechatId();
    int getProjectCount();
}
