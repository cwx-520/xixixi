package com.cwx.pma.controllers;


import com.cwx.pma.dao.ProjectRepository;
import com.cwx.pma.dao.StudentRepository;
import com.cwx.pma.dto.ChartData;
import com.cwx.pma.dto.StudentProject;
import com.cwx.pma.entities.Project;
import com.cwx.pma.entities.Student;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    @Value("${version}")
    private String version;

    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    StudentRepository studentRepository;


    @GetMapping("/")
    public String displayHome(Model model) throws JsonProcessingException {

        model.addAttribute("versionNumber",version);

       List<Project> projects = projectRepository.findAll();

       model.addAttribute("projects",projects);

        List<ChartData> projectStatusData = projectRepository.getProjectStatus();
        ObjectMapper objectMapper = new ObjectMapper();

        String jsonString= objectMapper.writeValueAsString(projectStatusData);
        model.addAttribute("projectStatusData",jsonString);

        List<StudentProject> studentsProjects = studentRepository.studentProjects();

        model.addAttribute("studentsProjects",studentsProjects);
       return"main/home";

    }
}
